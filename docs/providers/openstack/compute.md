##Using the Openstack Compute provider

Creating a client is straight-forward:

``` js
  var openstack = pkgcloud.compute.createClient({
    provider: 'openstack', // required
    username: 'your-user-name', // required
    password: 'your-password', // required
    authUrl: 'your identity service url' // required
  });
```

**Note:** *Due to variances between OpenStack deployments, you may or may not need a `region` option.*

[More options for creating clients](README.md)

### API Methods

**Servers**

#### client.getServers(callback)
Lists all servers that are available to use on your Openstack account

Callback returns `f(err, servers)` where `servers` is an `Array`

#### client.createServer(options, callback)
Creates a server with the options specified

Options are as follows:

```js
{
  name: 'serverName',   // required
  flavor: 'flavor1',    // required
  image: 'image1',      // required
  personality: []       // optional
  metadata: '',         // optional
  availability_zone: '' // optional
}
```
Returns the server in the callback `f(err, server)`

#### client.destroyServer(server, callback)
Destroys the specified server

Takes server or serverId as an argument  and returns the id of the destroyed server in the callback `f(err, serverId)`

#### client.getServer(server, callback)
Gets specified server

Takes server or serverId as an argument and returns the server in the callback
`f(err, server)`

#### client.rebootServer(server, options, callback)
Reboots the specifed server with options

Options include:

```js
{
  type: 'HARD' // optional (defaults to 'SOFT')
}
```
Returns callback with a confirmation

#### client.rebuildServer(server, options, callback)
Rebuilds the specifed server with options

Options include:

```js
{
  image: '45a01744-2bcf-4a23-ae88-63317f768a2f', // required; image ID or instance of pkgcloud.core.compute.Image
  accessIPv4: '123.45.67.89' // optional; IPv4 address of server
  accessIPv6: 'f0::09', // optional; IPv6 address of server
  adminPass: 'foobar', // optional; administrator password for the server
  metadata: { group: 'webservers' }, // optional; metadata key/value pairs
  personality: [ { path: '/etc/banner.txt', contents: 'ICAgICAgDQo' } ], // optional; personality files - path and contents
  'OS-DCF:diskConfig': 'AUTO' // optional; disk configuration value ("AUTO" | "MANUAL")  
}
```
Returns callback with a confirmation

#### client.liveMigrate(server, options, callback)
Live-migrates a server to a new host without rebooting.

Use the `host` parameter to specify the destination host. If this param is null, the scheduler chooses a host.

The `block_migration` parameter could be to auto so that nova can decide value of block_migration during live migration.

Set to True the `disk_over_commit` to enable over commit when the destination host is checked for available disk space. Set to False to disable over commit. This setting affects only the libvirt virt driver.

The `force` parameter forces a live-migration by not verifying the provided destination host by the scheduler.

Options are as follows:

```js
{
  host: 'computenode',    // required
  block_migration: true,  // required
  disk_over_commit: true, // required
  force: false            // optional
}
```
Returns the server's id and status in the callback `f(err, status)`


#### client.getServerRemoteConsole(server, options, callback)

Create server remote console.

Options are as follows:

```js
{
  protocol: 'vnc',  // required The valid values are vnc, spice, rdp, serial and mks.
  type: 'novnc',    // required The valid values are novnc, xvpvnc, rdp-html5, spice-html5, serial, and webmks.
}
```

Returns in remote console in the callback `f(err, remote_console)`.
Example:

```js
{
    remote_console: {
        protocol: "vnc",
        type: "novnc",
        url: "http://example.com:6080/vnc_auto.html?token=b60bcfc3-5fd4-4d21-986c-e83379107819"
    }
}
```

**Note about backwards compatiblity:**
For backwards compatibility, it is also possible to pass an image ID or instance of `pkgcloud.core.compute.Image` as the value of the `options` argument.

#### client.getVersion(callback)

Get the current version of the api returned in a callback `f(err, version)`

#### client.getLimits(callback)

Get the current API limits returned in a callback `f(err, limits)`

**flavors**

#### client.getFlavors(callback)

Returns a list of all possible server flavors available in the callback `f(err,
flavors)`

#### client.getFlavor(flavor, callback)
Returns the specified flavor of Openstack Images by ID or flavor
object in the callback `f(err, flavor)`

**images**

#### client.getImages(callback)
Returns a list of the images available for your account

`f(err, images)`

#### client.getImage(image, callback)
Returns the image specified

`f(err, image)`

#### client.createImage(options, callback)
Creates an Image based on a server

Options include:

```js
{
  name: 'imageName',  // required
  server: 'serverId'  // required
}
```

Returns the newly created image

`f(err, image)`

#### client.destroyImage(image, callback)
Destroys the specified image and returns a confirmation

`f(err, {ok: imageId})`

## Volume Attachments

Attaching a volume to a compute instance requires using an openstack compute client, as well as possessing a `volume` or `volumeId`. Detaching volumes behaves the same way.

#### client.getVolumeAttachments(server, callback)

Gets an array of volumeAttachments for the provided server.

`f(err, volumeAttachments)`

#### client.getVolumeAttachmentDetails(server, attachment, callback)

Gets the details for a provided server and attachment. `attachment` may either be the `attachmentId` or an object with `attachmentId` as a property.

`f(err, volumeAttachment)`

#### client.attachVolume(server, volume, callback)

Attaches the provided `volume` to the `server`. `volume` may either be the `volumeId` or an instance of `Volume`.

`f(err, volumeAttachment)`

#### client.detachVolume(server, attachment, callback)

Detaches the provided `attachment` from the server. `attachment` may either be the `attachmentId` or an object with `attachmentId` as a property. If the `volume` is mounted this call will return an err.

`f(err)`